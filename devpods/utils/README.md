# utils

[![CI Status](https://img.shields.io/travis/svvoff/utils.svg?style=flat)](https://travis-ci.org/svvoff/utils)
[![Version](https://img.shields.io/cocoapods/v/utils.svg?style=flat)](https://cocoapods.org/pods/utils)
[![License](https://img.shields.io/cocoapods/l/utils.svg?style=flat)](https://cocoapods.org/pods/utils)
[![Platform](https://img.shields.io/cocoapods/p/utils.svg?style=flat)](https://cocoapods.org/pods/utils)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

utils is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'utils'
```

## Author

svvoff, svvoff@gamil.com

## License

utils is available under the MIT license. See the LICENSE file for more info.

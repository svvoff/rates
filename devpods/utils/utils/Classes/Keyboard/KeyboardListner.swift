//
//  KeyboardListner.swift
//  Alamofire
//
//  Created by SOROKIN Andrey on 24/02/2019.
//

import UIKit

public protocol KeyboardListnerOutput: class {
    func keyboard(didChange state: KeyboardListner.State, startFrame: CGRect?, endFrame: CGRect?)
}

public final class KeyboardListner {
    public enum State {
        case willOpen
        case didOpen
        case willClose
        case didClose
    }

    public weak var output: KeyboardListnerOutput?
    public init(output: KeyboardListnerOutput?) {
        self.output = output
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(willShow(msg:)),
                                               name: UIWindow.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didShow(msg:)),
                                               name: UIWindow.keyboardDidShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(willDismiss(msg:)),
                                               name: UIWindow.keyboardWillHideNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(didDismiss(msg:)),
                                               name: UIWindow.keyboardDidHideNotification,
                                               object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc
    private func willShow(msg: Notification) {
        let startFrame = msg.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect
        let endFrame = msg.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect

        output?.keyboard(didChange: .willOpen, startFrame: startFrame, endFrame: endFrame)
    }

    @objc
    private func didShow(msg: Notification) {
        let startFrame = msg.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect
        let endFrame = msg.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        output?.keyboard(didChange: .didOpen, startFrame: startFrame, endFrame: endFrame)
    }

    @objc
    private func willDismiss(msg: Notification) {
        let startFrame = msg.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect
        let endFrame = msg.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        output?.keyboard(didChange: .willClose, startFrame: startFrame, endFrame: endFrame)
    }

    @objc
    private func didDismiss(msg: Notification) {
        let startFrame = msg.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect
        let endFrame = msg.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
        output?.keyboard(didChange: .didClose, startFrame: startFrame, endFrame: endFrame)
    }
}

//
//  NSLayoutConstraint.swift
//  utils
//
//  Created by SOROKIN Andrey on 23/02/2019.
//

import UIKit

public extension NSLayoutConstraint {
    public func activate() {
        if isActive == false {
            isActive = true
        }
    }
}

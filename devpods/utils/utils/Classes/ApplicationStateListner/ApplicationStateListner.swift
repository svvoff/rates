//
//  ApplicationStateListner.swift
//  Alamofire
//
//  Created by SOROKIN Andrey on 25/02/2019.
//

import UIKit

public protocol ApplicationStateListnerOutput: class {
    func applicationDidBecomeActive()
    func applicationWillResignActive()
}

public class ApplicationStateListner {
    public weak var output: ApplicationStateListnerOutput?
    public init(output: ApplicationStateListnerOutput?) {
        self.output = output

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(changeState(msg:)),
                                               name: UIApplication.willResignActiveNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(changeState(msg:)),
                                               name: UIApplication.didBecomeActiveNotification,
                                               object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc
    private func changeState(msg: Notification) {
        switch msg.name {
        case UIApplication.willResignActiveNotification:
            output?.applicationWillResignActive()
        case UIApplication.didBecomeActiveNotification:
            output?.applicationDidBecomeActive()
        default:
            break
        }
    }
}

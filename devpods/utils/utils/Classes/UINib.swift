

public extension UINib {
    public static func nib(for type: AnyClass) -> UINib? {
        let string = String(describing: type)
        return UINib(nibName: string, bundle: Bundle(for: type))
    }
}

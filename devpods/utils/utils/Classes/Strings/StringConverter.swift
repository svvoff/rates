//
//  StringValidator.swift
//  utils
//
//  Created by SOROKIN Andrey on 24/02/2019.
//

import Foundation

public protocol StringConverter {
    func convert(string: String) -> String
}

public final class NumberStringConverter: StringConverter {
    private let validCharSet = CharacterSet(charactersIn: "0123456789.")
    public init() {}
    public func convert(string: String) -> String {
        let inverted = validCharSet.inverted
        var input = string.components(separatedBy: inverted).joined()

        input = removeNeedlessDots(in: input)

        return input
    }

    private func removeNeedlessDots(in string: String) -> String {
        let hasDot = string.split(separator: ".").count > 1 || (string.hasPrefix(".") || string.hasSuffix("."))
        let hasOnlyOneDot = string.range(of: "..") == nil &&
            (string.split(separator: ".").count == 2 &&
                (!string.hasPrefix(".") && !string.hasSuffix("."))) &&
            !(string.hasPrefix(".") && string.hasSuffix(".")) ||
            string.range(of: "..") == nil &&
            (string.split(separator: ".").count == 1 &&
                (string.hasPrefix(".") && !string.hasSuffix(".") || (!string.hasPrefix(".") && string.hasSuffix("."))))

        if !hasDot || hasOnlyOneDot {
            return string
        }

        var input = string

        input = input.split(separator: ".").joined(separator: ".")

        var hasDotInside = false
        if input.split(separator: ".").count > 2 {
            var dot = input.split(separator: ".")
            dot = [dot[0], dot[1]]
            input = dot.joined(separator: ".")
            hasDotInside = true
        }

        if
            hasDotInside,
            (input.hasSuffix(".") || input.hasPrefix("."))  {
            input = input.trimmingCharacters(in: CharacterSet(charactersIn: "."))
        }


        return input
    }
}

//
//  Section.swift
//  ddm
//
//  Created by SOROKIN Andrey on 15/02/2019.
//

import Foundation

public struct Section {
    public let items: [ Item ]
    public init(items: [ Item ]) {
        self.items = items
    }

    public var count: Int {
        get {
            return items.count
        }
    }
}

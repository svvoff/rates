
import UIKit

public class Item {
    public let id: String
    public let height: Double
    public let entity: Any?
    public let configurator: CellConfigurator?

    public init(id: String, height: Double = Double(exactly: UITableView.automaticDimension) ?? 44.0, entity: Any? = nil, configurator: CellConfigurator? = nil) {
        self.id = id
        self.height = height
        self.entity = entity
        self.configurator = configurator
    }
}

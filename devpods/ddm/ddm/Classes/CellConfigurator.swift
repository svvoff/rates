//
//  CellConfigurator.swift
//  ddm
//
//  Created by SOROKIN Andrey on 23/02/2019.
//

import UIKit

public protocol CellConfigurator: class {
    func tableView(_ tableView: UITableView, configure cell: UITableViewCell, for indexPath: IndexPath, entity: Any?)

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, for indexPath: IndexPath)

    func tableView(_ tableView: UITableView, didSelectCellAt indexPath: IndexPath)
}

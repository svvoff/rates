//
//  Content.swift
//  ddm
//
//  Created by SOROKIN Andrey on 15/02/2019.
//

import Foundation

public struct Content {
    public let sections: [ Section ]
    public var count: Int {
        return sections.count
    }

    public init(sections: [Section]) {
        self.sections = sections
    }

    public func item(for indexPath: IndexPath) -> Item {
        return sections[indexPath.section].items[indexPath.row]
    }
}

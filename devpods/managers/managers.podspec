Pod::Spec.new do |spec|
  spec.name         = 'managers'
  spec.version      = '0.0.1'
  spec.license      = { :type => 'BSD' }
  spec.homepage     = 'https://github.com/svvoff/managers'
  spec.authors      = { 'Andrey Sorokin' => 'svvoff@gmail.com' }
  spec.summary      = 'managers'

  spec.ios.deployment_target = '12.0'

  spec.source       = { :path => '.' }
  spec.source_files = 'managers/Classes/**/*'
  spec.framework    = 'Foundation'

  spec.dependency 'Alamofire', '4.8.1'
  spec.dependency 'ObjectMapper', '~> 3.4'
  spec.dependency 'AlamofireObjectMapper', '~> 5.2'
  spec.dependency 'Swinject', '~> 2.5'
  spec.dependency 'dto'
end

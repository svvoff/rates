# managers

[![CI Status](https://img.shields.io/travis/svvoff/managers.svg?style=flat)](https://travis-ci.org/svvoff/managers)
[![Version](https://img.shields.io/cocoapods/v/managers.svg?style=flat)](https://cocoapods.org/pods/managers)
[![License](https://img.shields.io/cocoapods/l/managers.svg?style=flat)](https://cocoapods.org/pods/managers)
[![Platform](https://img.shields.io/cocoapods/p/managers.svg?style=flat)](https://cocoapods.org/pods/managers)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

managers is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'managers'
```

## Author

svvoff, svvoff@gamil.com

## License

managers is available under the MIT license. See the LICENSE file for more info.

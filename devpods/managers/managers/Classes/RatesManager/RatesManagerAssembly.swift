//
//  RatesManagerAssembly.swift
//  managers
//
//  Created by SOROKIN Andrey on 19/02/2019.
//

import Swinject

public final class RatesManagerAssembly: Assembly {
    public func assemble(container: Container) {
        container.register(RatesManager.self) { (resolver) -> RatesManager in
            return RatesManagerImp()
        }.inObjectScope(.transient)
    }
    public init() {}
}

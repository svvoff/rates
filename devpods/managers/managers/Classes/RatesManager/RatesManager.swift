

import Foundation
import Alamofire
import AlamofireObjectMapper
import dto

public protocol RatesManager: class {
    func fetchRates(with base: String, completion: @escaping (BaseRate?, Error?, ManagerRequest) -> Void) -> ManagerRequest
}

final class RatesManagerImp: RatesManager {
    let url: URL = URL(string: "https://revolut.duckdns.org/latest")!

    enum RatesManagerError: Error {
        case unknown

        var localizedDescription: String {
            switch self {
            case .unknown:
                return "something went wrong"
            }
        }
    }

    func fetchRates(with base: String, completion: @escaping (BaseRate?, Error?, ManagerRequest) -> Void) -> ManagerRequest {
        assert(base.isEmpty == false, "incorrect base")
        let request = Alamofire.request(url,
                                        method: .get,
                                        parameters: [ "base" : base ])
        let managerRequest = ManagerRequestImp(request: request)
        request.responseObject(queue: DispatchQueue.global(qos: .default)) { (response: DataResponse<BaseRateImp>) in
            if let baseRate = response.result.value {
                completion(baseRate, nil, managerRequest)
            } else {
                let error = response.error ?? RatesManagerError.unknown
                completion(nil, error, managerRequest)
            }
        }
        return managerRequest
    }
}

//
//  ManagerRequest.swift
//  managers
//
//  Created by SOROKIN Andrey on 26/02/2019.
//

import UIKit
import Alamofire

public protocol ManagerRequest: class {
    func cancel()
}

final class ManagerRequestImp: ManagerRequest {
    weak var request: Request?
    init(request: Request) {
        self.request = request
    }
    func cancel() {
        request?.cancel()
    }
}

# dto

[![CI Status](https://img.shields.io/travis/svvoff/dto.svg?style=flat)](https://travis-ci.org/svvoff/dto)
[![Version](https://img.shields.io/cocoapods/v/dto.svg?style=flat)](https://cocoapods.org/pods/dto)
[![License](https://img.shields.io/cocoapods/l/dto.svg?style=flat)](https://cocoapods.org/pods/dto)
[![Platform](https://img.shields.io/cocoapods/p/dto.svg?style=flat)](https://cocoapods.org/pods/dto)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

dto is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'dto'
```

## Author

svvoff, svvoff@gamil.com

## License

dto is available under the MIT license. See the LICENSE file for more info.

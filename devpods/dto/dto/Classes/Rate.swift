//
//  Rate.swift
//  Alamofire
//
//  Created by SOROKIN Andrey on 18/02/2019.
//

import Foundation
import ObjectMapper

public protocol Rate {
    var id: String { get }
    var rate: Double { get }
}

public struct RateImp: Rate {
    public let id: String
    public let rate: Double

    public init(id: String, rate: Double) {
        self.id = id
        self.rate = rate
    }
}

extension RateImp: ImmutableMappable {
    public init(map: Map) throws {
        id = try map.value("base")
        rate = try map.value("rate")
    }
}

//
//  BaseRate.swift
//  Alamofire
//
//  Created by SOROKIN Andrey on 18/02/2019.
//

import Foundation
import ObjectMapper


public protocol BaseRate {
    var base: String { get }
    var date: Date { get }
    var rates: [Rate] { get }
}

public struct BaseRateImp: BaseRate {
    public var base: String
    public var date: Date

    public var rates: [Rate]

}

extension BaseRateImp: ImmutableMappable {
    public init(map: Map) throws {
        base = try map.value("base")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        date = try map.value("date", using: DateFormatterTransform(dateFormatter: dateFormatter))
        
        let ratesTransform = TransformOf<[Rate], [String: Double]>(fromJSON: { (json) -> [Rate]? in
            guard let json = json else { return nil }
            var rates = [ Rate ]()
            for (key, value) in json {
                rates.append(RateImp(id: key, rate: value))
            }
            return rates
        }) { (rates) -> [String : Double]? in
            guard let rates = rates else { return nil }
            var dict = [String: Double]()
            for rate in rates {
                dict[rate.id] = rate.rate
            }
            return dict
        }
        rates = try map.value("rates", using: ratesTransform)
    }
}

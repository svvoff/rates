
import UIKit
import utils

public final class UnderlinedTextField: UITextField {

    private let unfocusedColor = UIColor.red

    private lazy var underline: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = unfocusedColor
        view.isUserInteractionEnabled = false
        return view
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        clipsToBounds = false
        addSubview(underline)
        configureConstraints(forUnderlineView: underline)
        layoutIfNeeded()
    }

    public override func becomeFirstResponder() -> Bool {
        underline.backgroundColor = self.tintColor
        return super.becomeFirstResponder()
    }

    public override func resignFirstResponder() -> Bool {
        underline.backgroundColor = unfocusedColor
        return super.resignFirstResponder()
    }

    private func configureConstraints(forUnderlineView underlineView: UIView) {
        underlineView.translatesAutoresizingMaskIntoConstraints = false
        underlineView.heightAnchor.constraint(equalToConstant: 1).activate()
        underlineView.topAnchor.constraint(equalTo: bottomAnchor).activate()
        underlineView.leftAnchor.constraint(equalTo: leftAnchor).activate()
        underlineView.rightAnchor.constraint(equalTo: rightAnchor).activate()
    }
}

# controls

[![CI Status](https://img.shields.io/travis/svvoff/controls.svg?style=flat)](https://travis-ci.org/svvoff/controls)
[![Version](https://img.shields.io/cocoapods/v/controls.svg?style=flat)](https://cocoapods.org/pods/controls)
[![License](https://img.shields.io/cocoapods/l/controls.svg?style=flat)](https://cocoapods.org/pods/controls)
[![Platform](https://img.shields.io/cocoapods/p/controls.svg?style=flat)](https://cocoapods.org/pods/controls)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

controls is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'controls'
```

## Author

svvoff, svvoff@gamil.com

## License

controls is available under the MIT license. See the LICENSE file for more info.

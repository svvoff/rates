
//
//  RatesManagerTest.swift
//  testTests
//
//  Created by SOROKIN Andrey on 24/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import XCTest
import Mockingjay
@testable import test
@testable import managers
@testable import dto

class RatesManagerTest: XCTestCase {

    let manager = RatesManagerImp()

    override func setUp() {
        let path = Bundle(for: type(of: self)).path(forResource: "EUR", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        stub(uri("https://revolut.duckdns.org/latest?base=EUR"), jsonData(data))
        stub(uri("https://revolut.duckdns.org/latest?base=RUB"), http(500))
    }

    func testEUR() {
        let exp = expectation(description: "eur")
        _ = manager.fetchRates(with: "EUR") { (rate, error, req) in
            XCTAssertNotNil(rate)
            XCTAssertNil(error)
            XCTAssertNotNil(req)
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1)
    }

    func testRUB() {
        let exp = expectation(description: "rub")
        _ = manager.fetchRates(with: "RUB") { (rate, error, req) in
            XCTAssertNil(rate)
            XCTAssertNotNil(error)
            XCTAssertNotNil(req)
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1)
    }
}

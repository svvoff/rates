//
//  ddmTests.swift
//  testTests
//
//  Created by SOROKIN Andrey on 18/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import XCTest
@testable import ddm

class ddmTests: XCTestCase {

    func testItem() {
        let item = Item(id: "id", height: 12, entity: [ "test" : "test" ])
        XCTAssert(item.id == "id")
        XCTAssert(item.height == 12)
        XCTAssert(item.entity != nil)
        guard let entity = item.entity else {
            return XCTAssert(false, "no entity")
        }
        guard let dict = entity as? [ String : String ] else {
            return XCTAssert(false, "incorrect type")
        }
        XCTAssert(dict == [ "test" : "test" ])
    }

    func testSection() {
        let section = Section(items: [ Item(id: "id") ])
        XCTAssert(section.count == 1)
    }

    func testContent() {
        let section = Section(items: [ Item(id: "id") ])
        let content = Content(sections: [ section ])
        XCTAssert(content.count == 1)
        let item = content.item(for: IndexPath(item: 0, section: 0))
        XCTAssert(item.id == "id")
    }
}

//
//  NumberStringConverterTest.swift
//  testTests
//
//  Created by SOROKIN Andrey on 24/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import XCTest
@testable import test
@testable import utils

class NumberStringConverterTest: XCTestCase {


    func testConverter() {
        let converter = NumberStringConverter()
        XCTAssert(converter.convert(string: "") == "")
        XCTAssert(converter.convert(string: "100.0") == "100.0")

        XCTAssert(converter.convert(string: "10 0") == "100")
        XCTAssert(converter.convert(string: "100..0") == "100.0")
        XCTAssert(converter.convert(string: "100.00.0") == "100.00")
        XCTAssert(converter.convert(string: ".100.0") == "100.0")
        XCTAssert(converter.convert(string: ".100.0.") == "100.0")
        XCTAssert(converter.convert(string: "....100.0...") == "100.0")
        XCTAssert(converter.convert(string: "100.....") == "100")

        XCTAssert(converter.convert(string: "100.") == "100.")
        XCTAssert(converter.convert(string: ".100.") == "100")
        XCTAssert(converter.convert(string: ".100") == ".100")
        XCTAssert(converter.convert(string: ".100.0") == "100.0")
        XCTAssert(converter.convert(string: ".100.0.") == "100.0")
        XCTAssert(converter.convert(string: "100.0.") == "100.0")

        XCTAssert(converter.convert(string: "100.0asdf:;;^^,,,6") == "100.06")
    }
}

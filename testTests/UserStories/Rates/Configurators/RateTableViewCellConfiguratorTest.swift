//
//  RateTableViewCellConfiguratorTest.swift
//  testTests
//
//  Created by SOROKIN Andrey on 24/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import XCTest
@testable import test

class RateTableViewCellConfiguratorTest: XCTestCase {

    func testConfigurator() {
        let configurator = RateTableViewCellConfigurator(base: "EUR", output: nil)
        configurator.rate = 0.5
        XCTAssert(configurator.rate == 0.5)
        configurator.updateAmount(with: 150)
        XCTAssert(configurator.amount == 75)

        configurator.rate = 0.8
        XCTAssert(configurator.rate == 0.8)
        XCTAssert(configurator.amount == 75)

        XCTAssert(configurator.base == "EUR")
    }
}



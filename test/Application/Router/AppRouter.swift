//
//  AppRouter.swift
//  test
//
//  Created by SOROKIN Andrey on 19/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit
import Swinject
import managers

protocol AppRouter {
    var entryPoint: UIViewController { get }
}

class AppRouterImp: AppRouter {
    var entryPoint: UIViewController

    private let assember: Assembler = Assembler([
        RatesManagerAssembly(),
        RatesAssembly()
        ])

    init() {
        guard let viewController = assember.resolver.resolve(RatesViewController.self) else {
            fatalError()
        }
        entryPoint = UINavigationController(rootViewController: viewController)
    }
}

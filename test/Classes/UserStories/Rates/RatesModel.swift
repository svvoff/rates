//
//  RatesModel.swift
//  test
//
//  Created by SOROKIN Andrey on 20/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Foundation
import managers
import ddm
import dto
import utils
import Reachability

protocol RatesModelInput: class {
    func viewDidLoad()
}

final class RatesModel {
    private let ratesManager: RatesManager

    weak var view: RatesViewInput?

    var createConfigurator: ((String) -> RateTableViewCellConfiguratorInput)!

    var base: String = "EUR"

    private let queue: DispatchQueue = DispatchQueue(label: "sync")

    private let reachability = Reachability()

    var timer: Timer?
    private let fetchInterval = TimeInterval(1)

    var appStateListner: ApplicationStateListner?

    var configurators = [ RateTableViewCellConfiguratorInput ]()
    var content: Content?

    var requests: [ManagerRequest] = []

    init(ratesManager: RatesManager) {
        self.ratesManager = ratesManager
        let timer = configuredTimer()
        fetchRates(with: self.base)
        self.timer = timer
        reachability?.whenUnreachable = { [weak self] (rechability) in
            self?.destroyTimer()
            self?.view?.set(title: "No Internet connection")
            self?.cancelAllRequests()
        }

        reachability?.whenReachable = { [weak self] (reachability) in
            if self?.timer == nil {
                self?.timer = self?.configuredTimer()
            }
            self?.view?.set(title: "Rates")
        }
        try? reachability?.startNotifier()
    }

    private func configuredTimer() -> Timer {
        let timer = Timer.scheduledTimer(withTimeInterval: fetchInterval,
                                         repeats: true,
                                         block: { [weak self] (_) in
                                            guard let self = self else { return }
                                            self.fetchRates(with: self.base)
        })
        RunLoop.main.add(timer, forMode: .common)
        return timer
    }

    private func cancelAllRequests() {
        for req in requests {
            req.cancel()
        }
        requests.removeAll()
    }

    private func fetchRates(with base: String) {
        let request = ratesManager.fetchRates(with: base) { [weak self] (rate, error, request) in
            guard let self = self else { return }
            self.queue.sync {
                var requests = self.requests
                for (i, req) in self.requests.enumerated() {
                    if req !== request {
                        req.cancel()
                    } else {
                        requests.removeSubrange(0...i)
                        break
                    }
                }
                self.requests = requests
            }
            if let _ = error {
                return
            }
            guard let rate = rate else { return }

            self.queue.sync {
                if self.content == nil {

                    self.createConfigurators(with: rate)
                    let content = self.configuredContent(with: self.configurators)
                    self.content = content
                    self.view?.content = content
                    DispatchQueue.main.async {
                        self.view?.hideIndicator()
                        self.view?.reload()
                    }
                } else {
                    self.updateConfigurators(with: rate)
                }
            }
        }
        requests.append(request)
    }

    private func createConfigurators(with baseRate: BaseRate) {
        let baseConfigurator = self.configurator(for: baseRate.base)
        baseConfigurator.rate = 1
        for rate in baseRate.rates {
            let configurator = self.configurator(for: rate.id)
            configurator.rate = rate.rate
            configurator.updateAmount(with: baseConfigurator.amount)
        }
    }

    private func updateConfigurators(with baseRate: BaseRate) {
        let baseConfigurator = configurators[0]
        if baseRate.base != baseConfigurator.base {
            return
        }
        for rate in baseRate.rates where rate.id != baseRate.base {
            let configurator = self.configurator(for: rate.id)
            configurator.rate = rate.rate
            configurator.updateAmount(with: baseConfigurator.amount)
        }
    }

    private func configuredContent(with configurators: [ RateTableViewCellConfiguratorInput ]) -> Content {
        var items = [ Item ]()

        for configurator in configurators {
            items.append(Item(id: String(describing: RateTableViewCell.self),
                              height: 50,
                              entity: configurator.rate,
                              configurator: configurator))
        }

        return Content(sections: [Section(items: items)])
    }

    private func configurator(for base: String) -> RateTableViewCellConfiguratorInput {
        var returning: RateTableViewCellConfiguratorInput?
        for configurator in configurators where configurator.base == base {
            returning = configurator
        }

        if returning == nil {
            let configurator = createConfigurator(base)
            configurators.append(configurator)
            returning = configurator
        }
        return returning!
    }

    private func rearrangeConfiguratorsIfNeeded(withSelected configurator: RateTableViewCellConfiguratorInput) {
        guard let index = configurators.firstIndex(where: {  $0.base == configurator.base }) else {
            fatalError()
        }

        if index != 0 {
            queue.sync {
                configurators.remove(at: index)
                configurators.insert(configurator, at: 0)
                let rate = configurator.rate
                for item in configurators {
                    item.rate = item.rate / rate
                    item.updateAmount(with: configurator.amount)
                }
                self.content = self.configuredContent(with: configurators)
                view?.content = self.content
            }
            DispatchQueue.main.async {
                self.view?.moveToTop(index: IndexPath(row: index, section: 0))
                self.view?.scrollToTop()
            }
        }
    }

    private func destroyTimer() {
        timer?.invalidate()
        timer = nil
    }

    deinit {
        timer?.invalidate()
    }
}

extension RatesModel: RatesModelInput {
    func viewDidLoad() {
        view?.showIndicator()
    }
}

extension RatesModel: RateTableViewCellConfiguratorOutput {
    func configuratorDidBeginEditing(_ configurator: RateTableViewCellConfiguratorInput) {
        self.base = configurator.base
        rearrangeConfiguratorsIfNeeded(withSelected: configurator)
    }
    
    func configurator(_ configurator: RateTableViewCellConfiguratorInput, didChange amount: Double) {
        for value in configurators where value.base != configurator.base {
            value.updateAmount(with: amount)
        }
    }
}

extension RatesModel: ApplicationStateListnerOutput {
    func applicationDidBecomeActive() {
        if
            timer == nil,
            (reachability?.connection == .wifi ||
                reachability?.connection == .cellular) {
            timer = configuredTimer()
        }
    }

    func applicationWillResignActive() {
        destroyTimer()
    }
}

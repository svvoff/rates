//
//  RateTableViewCell.swift
//  test
//
//  Created by SOROKIN Andrey on 21/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit

class RateTableViewCell: UITableViewCell {
    @IBOutlet weak var textField: UITextField?
    @IBOutlet weak var currencyCodeLabel: UILabel?
}

//
//  RateTableViewCellConfigurator.swift
//  test
//
//  Created by SOROKIN Andrey on 23/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import ddm
import dto
import utils

protocol RateTableViewCellConfiguratorOutput: class {
    func configurator(_ configurator: RateTableViewCellConfiguratorInput, didChange amount: Double)
    func configuratorDidBeginEditing(_ configurator: RateTableViewCellConfiguratorInput)
}

protocol RateTableViewCellConfiguratorInput: CellConfigurator {
    var amount: Double { get }
    var rate: Double { get set }
    var base: String { get }
    func updateAmount(with amount: Double)
}

final class RateTableViewCellConfigurator: RateTableViewCellConfiguratorInput {

    weak var output: RateTableViewCellConfiguratorOutput?
    let base: String

    var stringConverter: StringConverter!

    var amount: Double = 100
    var rate: Double = 1

    private weak var cell: RateTableViewCell?

    init(base: String, output: RateTableViewCellConfiguratorOutput?) {
        self.base = base
        self.output = output
    }

    func updateAmount(with amount: Double) {
        self.amount = amount * self.rate
        let displaingString = string(from: self.amount)
        DispatchQueue.main.async {
            self.cell?.textField?.text = displaingString
        }
    }

    private func string(from amount: Double) -> String {
        return String(format: "%.2f", self.amount)
    }
}

extension RateTableViewCellConfigurator: CellConfigurator {
    func tableView(_ tableView: UITableView, didSelectCellAt indexPath: IndexPath) {
        cell?.textField?.becomeFirstResponder()
    }

    func tableView(_ tableView: UITableView, configure cell: UITableViewCell, for indexPath: IndexPath, entity: Any?) {
        guard let cell = cell as? RateTableViewCell else {
            return
        }
        self.cell = cell
        cell.selectionStyle = .none

        cell.currencyCodeLabel?.text = base
        if let rate = entity as? Double {
            self.rate = rate
        }

        cell.textField?.text = string(from: amount)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? RateTableViewCell else {
            return
        }
        self.cell = cell
        cell.textField?.addTarget(self,
                                  action: #selector(textDidChange(textField:)),
                                  for: .editingChanged)
        cell.textField?.addTarget(self,
                                  action: #selector(textDidBeginEditing(textField:)),
                                  for: .editingDidBegin)
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, for indexPath: IndexPath) {
        self.cell?.textField?.removeTarget(self,
                                           action: #selector(textDidBeginEditing(textField:)),
                                           for: .editingChanged)
        self.cell?.textField?.removeTarget(self,
                                           action: #selector(textDidChange(textField:)),
                                           for: .editingDidBegin)
        self.cell = nil
    }
}

// MARK: - TextField
extension RateTableViewCellConfigurator {
    @objc
    private func textDidChange(textField: UITextField?) {
        let string = stringConverter.convert(string: textField?.text ?? "")
        textField?.text = string
        let amount = Double(string) ?? 0.0
        self.amount = amount
        output?.configurator(self, didChange: amount)
    }

    @objc
    private func textDidBeginEditing(textField: UITextField?) {
        output?.configuratorDidBeginEditing(self)
    }
}

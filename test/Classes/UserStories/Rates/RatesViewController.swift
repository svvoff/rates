//
//  RatesViewController.swift
//  test
//
//  Created by SOROKIN Andrey on 18/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import UIKit
import managers
import utils
import ddm

protocol RatesViewInput: class {
    var content: Content? { get set }

    func set(title: String)
    
    func reload()

    func moveToTop(index: IndexPath)
    func scrollToTop()

    func showIndicator()
    func hideIndicator()
}

final class RatesViewController: UIViewController {
    var model: RatesModelInput!
    
    private lazy var tableView: UITableView = {
        return UITableView(frame: .zero)
    }()

    internal var content: Content?
    private lazy var indicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .whiteLarge)
        view.color = .gray
        return view
    }()

    var keyboardListner: KeyboardListner?

    override func viewDidLoad() {
        super.viewDidLoad()
        model.viewDidLoad()
        navigationItem.title = "Rates"
        configure(tableView: tableView)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if indicator.superview == view {
            view.bringSubviewToFront(indicator)
        }
    }

    private func configure(tableView: UITableView) {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false

        tableView.topAnchor.constraint(equalTo: view.topAnchor).activate()
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).activate()
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).activate()
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).activate()

        tableView.dataSource = self
        tableView.delegate = self

        tableView.separatorStyle = .none

        tableView.keyboardDismissMode = .interactive
        
        registerCells(in: tableView)
    }

    private func registerCells(in tableView: UITableView) {
        let nib = UINib.nib(for: RateTableViewCell.self)
        tableView.register(nib,
                           forCellReuseIdentifier: String(describing: RateTableViewCell.self))
    }
}

extension RatesViewController: RatesViewInput {
    func moveToTop(index: IndexPath) {
        tableView.moveRow(at: index, to: IndexPath(row: 0, section: 0))
    }

    func reload() {
        tableView.reloadData()
    }

    func set(title: String) {
        navigationItem.title = title
    }

    func scrollToTop() {
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0),
                              at: .top,
                              animated: true)
    }

    func showIndicator() {
        if indicator.superview != view {
            view.addSubview(indicator)
            indicator.translatesAutoresizingMaskIntoConstraints = false
            indicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).activate()
            indicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).activate()
        }
        indicator.startAnimating()
    }

    func hideIndicator() {
        indicator.removeFromSuperview()
    }
}

extension RatesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content?.sections[section].count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let content = content else {
            return UITableViewCell()
        }
        let item = content.item(for: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: item.id,
                                                 for: indexPath)
        if let configurator = item.configurator {
            configurator.tableView(tableView,
                                   configure: cell,
                                   for: indexPath,
                                   entity: item.entity)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(content?.item(for: indexPath).height ?? 44)
    }
}

extension RatesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let content = content else { return }
        let item = content.item(for: indexPath)
        if let configurator = item.configurator {
            configurator.tableView(tableView,
                                   didEndDisplaying: cell,
                                   for: indexPath)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let content = content else { return }
        let item = content.item(for: indexPath)
        if let configurator = item.configurator {
            configurator.tableView(tableView, didSelectCellAt: indexPath)
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let content = content else { return }
        let item = content.item(for: indexPath)
        if let configurator = item.configurator {
            configurator.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
        }
    }
}

extension RatesViewController: KeyboardListnerOutput {
    func keyboard(didChange state: KeyboardListner.State, startFrame: CGRect?, endFrame: CGRect?) {
        var insets = tableView.contentInset
        var scrollInsets = tableView.scrollIndicatorInsets
        switch state {
        case .willClose:
            scrollInsets.bottom = 0
            insets.bottom = 0
            tableView.contentInset = insets
            tableView.scrollIndicatorInsets = scrollInsets
        case .willOpen:
            if let bottom = endFrame?.height {
                insets.bottom = bottom
                scrollInsets.bottom = bottom
            }
            tableView.contentInset = insets
            tableView.scrollIndicatorInsets = scrollInsets
        default:
            break
        }
    }
}

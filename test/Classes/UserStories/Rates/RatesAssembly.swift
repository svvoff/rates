//
//  RatesAssembly.swift
//  test
//
//  Created by SOROKIN Andrey on 19/02/2019.
//  Copyright © 2019 test. All rights reserved.
//

import Swinject
import managers
import utils

final class RatesAssembly: Assembly {
    private var container: Container!
    func assemble(container: Container) {
        self.container = Container(parent: container)

        container.register(StringConverter.self, name: "number") { (_) -> StringConverter in
            return NumberStringConverter()
        }

        container.register(RateTableViewCellConfiguratorInput.self) { (resolver, base: String, output: RateTableViewCellConfiguratorOutput?) -> RateTableViewCellConfiguratorInput in
            let configurator = RateTableViewCellConfigurator(base: base, output: output)
            configurator.stringConverter = self.numbersStringConverter()
            return configurator
            }.inObjectScope(.transient)

        container.register(ApplicationStateListner.self) { (_, output: ApplicationStateListnerOutput) -> ApplicationStateListner in
            return ApplicationStateListner(output: output)
        }

        container.register(RatesModel.self) { (resolver) -> RatesModel in
            guard let ratesManager = resolver.resolve(RatesManager.self) else {
                fatalError("where is my \(RatesManager.self)?")
            }
            let model = RatesModel(ratesManager: ratesManager)
            model.appStateListner = resolver.resolve(ApplicationStateListner.self, argument: model as ApplicationStateListnerOutput)
            model.createConfigurator = { [weak model] (base: String) -> RateTableViewCellConfiguratorInput in
                guard let configurator = resolver.resolve(RateTableViewCellConfiguratorInput.self, arguments: base, model as RateTableViewCellConfiguratorOutput?) else { fatalError() }
                return configurator
            }
            return model
        }

        container.register(KeyboardListner.self) { (resolver, output: KeyboardListnerOutput) -> KeyboardListner in
            return KeyboardListner(output: output)
        }

        container.register(RatesViewController.self) { (resolver) -> RatesViewController in
            guard let model = resolver.resolve(RatesModel.self) else {
                fatalError("where is my \(RatesModelInput.self)?")
            }
            let controller = RatesViewController()
            controller.keyboardListner = resolver.resolve(KeyboardListner.self,
                                                          argument: controller as KeyboardListnerOutput)
            controller.model = model
            model.view = controller
            return controller
        }
    }

    private func numbersStringConverter() -> StringConverter {
        guard let converter = container.resolve(StringConverter.self, name: "number") else {
            fatalError()
        }
        return converter
    }

    func ratesScreen() -> UIViewController {
        guard let controller = container.resolve(RatesViewController.self) else { fatalError() }
        return controller
    }
}
